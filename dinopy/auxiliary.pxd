# -*- coding: utf-8 -*-
from dinopy.definitions cimport *
from libc.math cimport pow as cpow
from libc.math cimport log2 as clog2
from libc.math cimport ceil as cceil
