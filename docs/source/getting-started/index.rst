=================
 Getting Started
=================

:Release: |version|
:Date: |today|

.. toctree::
    :maxdepth: 2

    introduction
    first-steps
    examples
