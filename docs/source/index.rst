.. include:: ../../README.rst

Contents
========

.. toctree::
   :maxdepth: 2

   getting-started/index
   encoding 
   autodoc/modules
   changelog
   faq
    

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

