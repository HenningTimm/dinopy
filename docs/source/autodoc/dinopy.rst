dinopy package
==============

.. toctree::
   :maxdepth: 4

   auxiliary
   conversion
   creader
   definitions
   exceptions
   fai_io
   fasta_writer
   fasta_reader
   fastq_writer
   fastq_reader
   input_opener
   nameline_parser
   processors
   sambam
   sam_reader
   sam_writer
   shape
   shaping

