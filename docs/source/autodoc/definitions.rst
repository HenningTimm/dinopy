===========================
 dinopy.definitions module
===========================

.. automodule:: dinopy.definitions
    :members:
    :undoc-members: 
    :show-inheritance:
    :special-members: __getitem__
    :exclude-members: add, keys, values
