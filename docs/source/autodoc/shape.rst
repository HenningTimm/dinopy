=====================
 dinopy.shape module
=====================

.. autoclass:: dinopy.shape.Shape
    :members: is_solid
