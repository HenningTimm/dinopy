Henning Timm

Genome Informatics, Institute of Human Genetics, Faculty of Medicine, University of Duisburg-Essen
Bioinformatics for High-Throughput Technologies, Algorithm Engineering, TU Dortmund

henning.timm@tu-dortmund.de

https://bitbucket.org/HenningTimm


Till Hartmann

Genome Informatics, Institute of Human Genetics, Faculty of Medicine, University of Duisburg-Essen

till.hartmann@tu-dortmund.de

https://bitbucket.org/tillux/
